---
layout: markdown_page
title: "GitLab Release Posts"
description: "Guidelines to create and update release posts"
---

<br>

### Release Blog Posts Handbook
{:.no_toc}

----

## On this page
{:.no_toc}

- TOC
{:toc}

----

## Release posts

GitLab releases a new version every 22nd of each month,
and announces it through monthly release posts.

Patch and security issues are addressed more often,
whenever necessary.

- For a list of release posts, please check the
category [release](/blog/categories/release/).
- For a list of security releases, please check
the category [security release](/blog/categories/security-release/).
- For a list of features per release, please check
the [release list](/release-list/).
- For all named changes, please check the changelog
for [GitLab CE](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CHANGELOG.md)
and [GitLab EE](https://gitlab.com/gitlab-org/gitlab-ee/blob/master/CHANGELOG.md).
- See also [release managers](/release-managers/).

### Templates

To start a new release post, please choose one of
these templates, and follow their instructions
to insert content. Please make sure to use
the most recent template available.

- [Monthly release](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/doc/release_blog_template.html.md)
- [Patch release](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/doc/patch_release_blog_template.html.md)
- [Security release](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/doc/security_release_blog_template.html.md)

For patch and security releases, please make sure
to specify them in the title, add the correct [category](../#categories):

- Patch releases: 
  - `title: "GitLab Patch Release: x.x.x and x.x.x"`
  - `categories: release`
- Security releases:
  - `title: "GitLab Security Release: x.x.x and x.x.x"`
  - `categories: security release`

----

## Monthly releases

Monthly releases have a special layout, developed
exclusively for them,
[introduced](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/4780)
in March 2017 for the release of
[GitLab 9.0](/2017/03/22/gitlab-9-0-released/).

### Getting started

To get started, add content to the
[monthly release template](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/doc/release_blog_template.html.md),
and create a merge request to make the post
available to receive contributions from the team.

Please use the [merge request template](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/.gitlab/merge_request_templates/Release-Post.md) to add task lists and set due dates accordingly:

![release post MR template](release-post-mr-template.png){:.shadow}

Monthly release posts are created in three stages:

- **General contributions**: everyone can contribute!
In this stage, team members will add features and their
respective images, videos, etc
- **Content review**: PMs check and review the content,
writers/editors copyedit anything necessary
- **Styling**: tech writing/frontend/ux perform a
structural check and apply special styles

To be able to finish our work in time, with no rush,
each stage will have its due date.

### Due dates

To having the release post well written and ready in
time for the release date, please set due dates for:

- [General contributions](#general-contributions)
from the team: 6th working day before the 22nd
- [Content review](#content-review) (PMs, copyedit,
last review): 4th working day before the 22nd
- [Styling](#styling): 1st working day before the 22nd

### General contributions

Added by the team until the 6th working day before
the 22nd. Please fill all the sections:

- Introduction
- MVP
- Webcast
- Upgrade Barometer
- Performance improvements
- Omnibus improvements
- Deprecations
- Extras (when necessary)
- Features

For every feature, please add:

- Feature name
- Available in (CE/EES/EEP)
- Feature weight (top, primary, secondary)
- Documentation link
- Feature description, related images, and videos

Write the description of every feature as you do
to regular blog posts. Please write according to
the [markdown guide](/handbook/product/technical-writing/markdown-guide/).

### Content review

The content review is performed by product managers (PMs),
who will check if everything is in place, and if there's
nothing missing. Technical writers/editors will follow with
copyedit and check for grammar, spelling, and typos.
Please follow the checklist in the MR description to
guide you through the review.

The review should be finished by the 4th working day before the 22nd.

### Styling

Once the post is filled with content, it's time to structure
them with HTML and CSS to [apply the correct styles](#custom-styles).
Styles will be applied either by the technical writing,
UX, or frontend team, between the 3rd and the 1st working
day before the 22nd.

The styles applied in markdown consist of HTML elements
and [custom classes](/handbook/product/technical-writing/markdown-guide/#classes-ids-and-attributes).
Attributes and ids can also be customized to enhance
readability and navigation.

----

## Monthly release post structural check

Start with a structural check, and apply the styles
later. Please follow the task list in the MR description.

### Filename

Start checking the filename and extension:

```
AAAA-MM-22-gitlab-X-X-released.html.md
```

### Frontmatter

Check the content of the frontmatter. Look for each
entry, wrap text with double quotes and paths with
single quotes, to prevent the page to break due to special chars:

```yaml
---
release_number: "X.X"
title: "GitLab X.X Released with XXX and YYY"
author: Name Surname
author_gitlab: gitlab.com-username
author_twitter: twitter-username
categories: release
image_title: '/images/X_X/X_X-cover-image.ext'
description: "GitLab 9.0 Released with XXX, YYY, ZZZ, KKK, and much more!"
twitter_image: '/images/tweets/gitlab-X-X-released.jpg'
extra_css:
  - release-posts.css
extra_js:
  - release-posts.js
---
```

#### Cover image

If the post doesn't have a cover image yet, please
add one, and don't forget to add a [reference](../#cover-image)
to it [at the very end](#cover-image-reference) of the post.

#### Social sharing image

It's recommended to add a [social sharing image](/handbook/marketing/social-marketing/#defining-social-media-sharing-information)
to the blog post. It's the image that will display on
social media feeds whenever the link to the post is shared.
The image should be placed under `source/images/tweets/`
and named after the post's filename (`gitlab-X-X-released.png`).

### HTML parser

To be able to mix markdown and HTML markup, add the
[parser](https://about.gitlab.com/handbook/product/technical-writing/markdown-guide/#mix-html--markdown-markup)
tag to the beginning of the post, immediately after the frontmatter:


```md
# frontmatter
---

{::options parse_block_html="true" /}
```

If the post holds indented HTML tags, they might not
render correctly, or be escaped, for the use of the parser.
In that case, you can "close" the parser tag and open
again whenever necessary:

```md
{::options parse_block_html="true" /}

... text to parse

{::options parse_block_html="false" /}
```

### Images and videos

Looks for videos and images through the content.
Make sure every image has an
[alternative text](/handbook/product/technical-writing/markdown-guide/#image-alt-text),
and that every [video iframe is wrapped into a figure tag](/handbook/product/technical-writing/markdown-guide/#videos), as in:

```html
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/PoBaY_rqeKA" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
```

The `<figure>` element is recommended for semantic SEO
and the `video_container` class will assure the video
is displayed responsively.

Check if all images have the class
[`{:.shadow}`](/handbook/product/technical-writing/markdown-guide/#shadow)
applied to them. If the image already have shadow, do not apply this class.

----

## Monthly release post custom styles
{:#custom-styles}

The following HTML tags and CSS classes needs to be applied
to the monthly release post.

### One column layout
{:#one-column}

The HTML element `<section>` contributes to semantic SEO,
to organize the content is different sections of the post.
The class `release-post-section` will display the content
center-aligned in one column. Apply it to the **introduction**,
and to every part of the post that you need to display in one column:

```html
<section class="release-post-section">
... content in regular markdown...
</section>
```

### Two columns layout
{:#two-columns}

To display the **primary features**, use a section with the class
`release-row` wrapping the content of the two columns (`<div>`s)
in a row. To display images on the left, we'll need further classes, as explained
below. Alternate them, repeating the sections as many times
as necessary for all primary features.

#### Text on the left, image on the right

![primary features - columns - img right](main-img-right.png){:.shadow}

```md
<section class="release-row">
<div class="column">

## Feature XXX ce ee

Feature description (regular markdown)

</div>
<div class="column">

![image](link)

</div>
</section>
```

#### Image on the left, text on the right

![primary features - columns - img left](main-img-left.png){:.shadow}

```md
<section class="release-row image-left">
<div class="column text-column">

## Feature XXX ce ee

Feature description (regular markdown)

</div>
<div class="column image-column">

![image](link)

</div>
</section>
```

#### Feature with no images

When a primary feature does not contain a related image,
you can either display it in a section with [one column](#one-column) or
move it to a secondary feature section.

#### Feature with multiple images

When there's more than one image for a single feature, put them
together in their designated column:

![multiple images for a feature](main-multi-img.png){:.shadow}

Do not indent divs, it will not render correctly if you do so.
{:.alert alert-info}

### Badges layout

Every feature should be accompanied by a badge (CE, EES, EEP).
To display them, use the content of the template `available in: (CE/EES/EEP)`,
adding the acronyms to each heading. They can be applied
to `h2` and `h3` elements.

- `## Feature XXX ce ee` will display the badges
`CE` and `EE` (use it for every feature available in GitLab CE)
- `### Feature YYY ees` will display the badge
`EES` (use it for features available in EES and EEP)
- `## Feature ZZZ eep` will display the badge
`EEP` (use it for features available in EEP only)

### Loose headings

If needed, you can add "loose headings" in the text,
wrapping them into one column divs:

```md
<div class="release-post-section text-center zero-bottom-margin">
## Loose heading
</div>
```

----

## Monthly release post sections

To build the release post, it's necessary to style each
of the following sections, in the following order.
If `extras` are present, the person tackling the
styling stage will figure out the best place to display them.

### Introduction

Start the post by adding the [html-markown parser](/handbook/product/technical-writing/markdown-guide/#mix-html--markdown-markup) (`{::options parse_block_html="true" /}`), 
then wrap the intro in [one column](#one-column) section:

```html
{::options parse_block_html="true" /}

<section class="release-post-section">

Introductory paragraph (regular markdown)

<!-- more -->

Introduction (regular markdown)

</section>
```

### MVP

To display the MVP of the month, use the information
provided in the template, and adjust it to its style.

```md
- Name and Surname:
- GitLab.com handle: https://gitlab.com/username
- Contributed to:
- Short description:
- Merge request link:
```

```md
<section class="mvp gray-section">
<div class="release-post-section">

![mvp-badge](/images/mvp_badge.png)

## This month's Most Valuable Person ([MVP](https://about.gitlab.com/mvp/)) is [Name Surname](https://gitlab.com/username)

Name made it possible to [feature-name](merge-request-link). Short description. Thanks Name!

</div>
</section>
```

### TOP and primary features

Start with the top feature wrapped into
[one column](#one-column) section.

```html
<section class="release-post-section">

... TOP feature ...

</section>
```

Follow with the remaining primary features in [two columns](#two-columns), with
and `<hr>` at the end of each section (`<hr>` == `----`):

```html
<section class="release-row">
<div class="column">

... primary feature x ....

</div>
<div class="column">

![image](link)

</div>
</section>
----
<section class="release-row image-left">
<div class="column text-column">

... primary feature y ...

</div>
<div class="column image-column">

![image](link)

</div>
</section>
----
```

Use one block for each two primary features (text-image + image-text).

### Secondary features

Start this part of the blog post with the [loose heading](#loose-headings):

```md
<div class="release-post-section text-center zero-bottom-margin">
## Other Improvements in GitLab X.X
</div>
```

To display **secondary features**, use the same logic as described
previously for [two columns](#text-on-the-left-image-on-the-right).
The difference is that images will be placed among the content,
not on the right or left of the text. To do so, use two columns
in a row and add all the features in between:

```md
<section class="release-row align-top divider">
<div class="column">
### Feature XXX ce ee

Feature description

![image](link)

### Feature YYY ees

Feature description

![image](link)

<!-- end of left-column -->
</div>
<div class="column">
### Feature ZZZ eep

Feature description

![image](link)

### Feature KKK ce ee

Feature description

![image](link)

<!-- end of right-column -->
</div>
</section>
```

The tricky part is to balance the content between the
columns to make them finish as near to a baseline as possible.

![secondary features - baseline](other-features.png){:.shadow}

**Note:** The classes `.align-top` and `.divider` are present to make sure the
content is aligned to the top (not center-aligned), and to add a vertical
divider between the two columns to improve readability.
{:.note}

### Deprecations

Use [one column](#one-column) layout:

```md
<section class="release-post-section">

## Deprecations
{:.text-center}

### XXX Deprecation

Description, link to issue or MR.

Due: **March 31st**, 2017 at 23:59h UTC.

</section>
```

For multiple deprecations, use multiple `h3`s:

```md
<section class="release-post-section">

## Deprecations
{:.text-center}

### XXX Deprecation

Description, link to issue or MR.

Due: **March 31st**, 2017 at 23:59h UTC.

### YYY Deprecation

Description, link to issue or MR.

Due: **August 2017**.

</section>
```


### Upgrade barometer

Use [one column](#one-column) layout:

```
<section class="release-post-section">

## Upgrade barometer
{:.text-center}

Description
</section>
```

### Last section

Finish the post with the last gray section (copy-paste):

```md
<section class="gray-section">
<div class="release-row align-top">
<div class="column">
### Changelog

Please check out the changelog to see all the named changes:

- [GitLab CE](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CHANGELOG.md)
- [GitLab EE](https://gitlab.com/gitlab-org/gitlab-ee/blob/master/CHANGELOG.md)

### Installing

If you are setting up a new GitLab installation please see the
[download GitLab page](/installation/).

### Updating

Check out our [update page](/update/).

</div>
<div class="column">

### GitLab Products

We offer four different [products](/products/) for you and your company:

- {: #ce} **GitLab Community Edition (CE)**: [Open source](https://gitlab.com/gitlab-org/gitlab-ce), self-hosted solution of GitLab. Ideal for personal projects or small teams with minimal user management and workflow control needs. Every feature available in GitLab CE, is also available on GitLab Enterprise Edition (Starter and Premium), and GitLab.com.
- {: #ee} **[GitLab Enterprise Edition](/gitlab-ee/) (EE)**: [Open core](https://gitlab.com/gitlab-org/gitlab-ee/), self-hosted, fully featured solution of GitLab. Available in two different subscriptions:
  - {: #ees} **GitLab Enterprise Edition Starter (EES)**: Ideal for co-located teams who need additional security and workflow controls for their professional projects.
  - {: #eep} **GitLab Enterprise Edition Premium (EEP)**: Ideal for distributed teams who need advanced workflow controls, premium features, High Availability, and Premium Support.
- {: #gitlab-dot-com} **[GitLab.com](/gitlab-com/)**: SaaS GitLab solution, with [free and paid subscriptions](https://about.gitlab.com/gitlab-com/). GitLab.com is hosted by GitLab, Inc. Ideal for individuals who want to get their projects up and running quickly. Administrated by GitLab (users don't have access to admin settings).

</div>
</div>
</section>
```

### Cover image reference

Add a reference to the cover image within a [one-column](#one-column)
section. Style it with the
[`.note` class](/handbook/product/technical-writing/markdown-guide/#note)
and make it center-aligned with
[`.text-center`](/handbook/product/technical-writing/markdown-guide/#text-align):

```md
<section class="release-post-section">
[Cover image](https://www.pexels.com/photo/landscape-nature-mountain-lake-37650/) licensed under [CC0](https://www.pexels.com/photo-license/).
{:.note .text-center}
</section>
```

<style>
  pre { margin-bottom: 20px; }
</style>
