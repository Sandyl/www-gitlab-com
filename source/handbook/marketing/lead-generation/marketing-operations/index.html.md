---
layout: markdown_page
title: "Marketing Operations"
---
Welcome to the Marketing Operations Handbook.

[Up one level to the Demand Generation Handbook](/handbook/marketing/demand-generation/)    

----

## On this page
{:.no_toc}

- TOC
{:toc}

----

## Tools We Use

Our current marketing tech stack consists of the following tools:

* Marketo
    * Marketing Automation tool used to communicate and manage customers and prospects
* Google Analytics
    * Web analytics tool used to measure campaign and website performance.
* Google Tag Manager
    * Tag management
* Piwik
    * Open source web analytics platform used to measure web performance
* Unbounce
    * Landing Page Tool
* Salesforce
    * CRM used to manage all customer and prospect data.  Used for campaign reporting.
* Infer
    * Lead scoring
* On24
    * Webcast software

## Marketo Training Videos

Here are links to the recordings of Marketo trainings that we've done:

* [Email Creation and Tokens](https://drive.google.com/open?id=0B1_ZzeTfG3XYZjUwa3ZFb2ZWeWc)
* [How to Use Smart Lists and Reporting](https://drive.google.com/open?id=0B1_ZzeTfG3XYcGV2VUFiR0dNaWM)
